package classRoom.pathfinding;

import java.util.ArrayList;

/**
 * @author pieslicer
 */
public class Node implements Comparable<Node>{

    // The node before this
    public Node predecessor;

    // Where are we going?
    public int goalIndex;

    // My index on the map, assuming
    //
    public int index;

    // dimensions of map
    private final int rows, cols;

    // A* specific variables
    public int cost; // the cost to get here, defaulted in 8 - puzzle problem to 0
    public int h; // the length from current position to goal.
    public int f; // the sum of cost and h

    public Node(int _index, Node _predecessor, int _goal, int _rows, int _cols) {
        predecessor = _predecessor;
        goalIndex = _goal;
        index = _index;

        rows = _rows;
        cols = _cols;

        // if this node has a predecessor..
        if (_predecessor != null) {
            this.cost = predecessor.cost + 1;
        } else {
            this.cost = 0;
        }

        // This is solely for clarifying reasons.
        this.h = manhattanDistance();
        this.f = cost + h;
    }

    /**
     *
     * Implemented for the sole purpose of
     * ArrayList.contains() implementation
     */
    public boolean equals (Object o){

        if (o instanceof Node) {

            Node n = (Node)o;

            if (this.index == (n.index)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Used for Collections.sort()
     * In our case, we only compare the f values,
     * which is the cost + the heuristic function.
     * (Manhattan or City block - distance)
     */
    public int compareTo(Node n) {
        if (this.f > n.f)
            return 1;
        if (this.f < n.f)
            return -1;
        return 0;
    }

    /**
     * The Manhattan or City block distance from node to goal
     */
    private int manhattanDistance() {
        return Math.abs(index % cols - goalIndex % cols) + Math.abs(index / cols - goalIndex / cols);
    }

    /**
     * Outputs this node in a pretty way.
     */
    public String format() {
        String s = "";

        s += "NODE: (" + (index % cols) + ", " + (index / cols) + ") - ";
        s += "cost: " + cost + ", ";
        s += "heuristic: " + h + ", ";
        s += "sum of cost+heuristic: " + f + ".";

        return s;
    }

    /**
     * Returns a list of this nodes neighbours.
     */
    public ArrayList<Node> getNeighbours() {
        ArrayList<Node> v = new ArrayList<Node>();

        switch (index % cols) {
            case 0:
                v.add(new Node(index + 1, this, goalIndex, rows, cols));
                break;
            case 15:
                v.add(new Node(index - 1, this, goalIndex, rows, cols));
                break;
            default:
                v.add(new Node(index + 1, this, goalIndex, rows, cols));
                v.add(new Node(index - 1, this, goalIndex, rows, cols));
                break;
        }

        switch (index / cols) {
            case 0:
                v.add(new Node(index + cols, this, goalIndex, rows, cols));
                break;

            case 19:
                v.add(new Node(index - cols, this, goalIndex, rows, cols));
                break;

            default:
                v.add(new Node(index + cols, this, goalIndex, rows, cols));
                v.add(new Node(index - cols, this, goalIndex, rows, cols));
                break;
        }

        return v;
    }
}
