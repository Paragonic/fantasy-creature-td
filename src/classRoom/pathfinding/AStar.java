package classRoom.pathfinding;

import java.util.Collections;
import java.util.ArrayList;

import classRoom.se.paragonic.Utils;

/**
 * @author pieslicer
 */
public class AStar {

    // Lists for A* algorithm
    private ArrayList<Node> openList;
    private ArrayList<Node> closedList;

    // columns and rows of map
    private int rows, cols;

    // CONSTRUCTOR.
    public AStar(int _rows, int _cols) {
        // set map columns and rows
        rows = _rows;
        cols = _cols;
    }

    /**
     * Attempts to find the shortest path
     * possible from point a to b within the
     * given map. Returned is the Node at the
     * goal - if no path is possible the algorithm
     * returns null;
     */
    public Node findPath(int[][] startMap) {
        // Init lists needed for a* pathfinding
        openList = new ArrayList<Node>();
        closedList = new ArrayList<Node>();

        // Set goal (find 3 in map)
        int goalIndex = findIndex(startMap, 3);

        // Set start (find 2 in map)
        Node startNode = new Node(findIndex(startMap, 2), null, goalIndex, rows, cols);
        Node currentNode = startNode;

        openList.add(currentNode);

        // While there are Nodes not checked left
        while (openList.size() > 0) {
            // Sort the list with regard to 'f-value',
            // i.e. heuristic (manhattan / city block-distance (see Node.java))
            // and the cost (length of path traversal) of the previous nodes.
            Collections.sort(openList);
            currentNode = openList.get(0);

            // If the current node is the goal we have found
            // one of the shortest path to goal.
            if (currentNode.index == goalIndex) {
                Utils.print("Reconstructing!");
                // Output the path (4 is path traversal, 1 is blocked tile)
                //reconstruct(currentNode, startMap);
                return currentNode;
            }

            // Remove the current node from checking
            // and add it to the checked nodes list.
            openList.remove(currentNode);
            closedList.add(currentNode);

            // Collect the neighbours of current node.
            ArrayList<Node> nl = currentNode.getNeighbours();

            // For each neighbour ..
            for (int i = 0; i < nl.size(); ++i) {

                // if the current neighbour is a wall, continue
                if (startMap[nl.get(i).index / cols][nl.get(i).index % cols] == 1) {
                    Utils.print("passableMap[" + (nl.get(i).index / cols) +  "][" + (nl.get(i).index % cols) + "] was busy!");
                    continue;
                }

                // if the current neighbour has already been checked, continue
                if (closedList.contains(nl.get(i)))
                    continue;

                // if the current neighbour isn't in openlist, add it.
                if (!openList.contains(nl.get(i)))
                    openList.add(nl.get(i));

            }
        }

        // if we get here there is no path available from startNode to goalNode.
        Utils.print("A* algorithm failed!");
        return null;
    }

    /**
     * looks for a in the 2D-array map and returns the index if found.
     */
    private int findIndex(int[][] map, int a) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; ++j) {
                if (map[i][j] == a)
                    return i * cols + j;
            }
        }

        return -1;
    }

    /**
     * Ugly way of displaying found path.
     */
    private void reconstruct(Node _n, int[][] map) {

        Node n = _n;
        String row;

        while (n.predecessor != null) {
            map[n.index / cols][n.index % cols] = 4;
            n = n.predecessor;
        }

        for (int i = 0 ; i < rows; i++) {
            row = "";
            for (int j = 0; j < cols; j++) {
                row += " " + Integer.toString(map[i][j]);
            }
            Utils.print(row);
        }
    }


}
