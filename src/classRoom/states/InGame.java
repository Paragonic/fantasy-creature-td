package classRoom.states;

import java.util.ArrayList;

import classRoom.gameObjects.species.*;
import classRoom.pathfinding.AStar;
import classRoom.pathfinding.Node;
import classRoom.se.paragonic.Utils;
import classRoom.gameObjects.Enemy;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TiledMap;

public class InGame extends BasicGameState
{
    // A neat way of keeping track of where in the game we are.
    private enum GameState {
        IDLE,
        PLAY,
        PLACE_TOWERS,
        OVER,
    }

    // Used for keeping track of time.
    private long startTimerTime = 0;

    // To let this state know where in the game we are. Did someone die? Pause? Who knows.
    private GameState gameState = GameState.PLAY;

    // The id of this state
    private final int id = 1;

    // The map as imported
    private TiledMap testMap;

    // The map represented as a 2D-array
    private int[][] passableMap;

    // Used to direct all enemies.
    private ArrayList<int[]> optimalPath;

    // Keep track of enemies on the screen;
    public static ArrayList<Enemy> enemies;

    // TEMPORARY VARIABLES
    private float x, y;
    private Animation sprite, right, left, down, up;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {

        // Load map
        testMap = new TiledMap("data/test.tmx");
        // set passableMap
        passableMap = new int[20][16];

        // set start and end points.
        passableMap[0][7] = 2; // start
        passableMap[19][7] = 3;

        for (int i = 0; i < passableMap[1].length - 1; ++i) {
            passableMap[1][i] = 1;
            passableMap[3][i + 1] = 1;
            passableMap[5][i] = 1;
            passableMap[7][i + 1] = 1;
            passableMap[9][i] = 1;
            passableMap[11][i + 1] = 1;
            passableMap[13][i] = 1;
            passableMap[15][i + 1] = 1;
            passableMap[17][i] = 1;
        }

        // Calculate shortest path and output the time of execution
        long startTime = System.currentTimeMillis();

        long endTime = System.currentTimeMillis();
        System.out.println("It took " + (endTime - startTime) + " milliseconds to find the shortest path!");


        // THIS IS SOLELY FOR TESTING. THIS WON'T BE HERE IN ACTUAL GAME.

        // Set pos of char
        x = 32*1;
        y = 32*1;

        enemies = new ArrayList<Enemy>();
        createAnimations();

        startGame();
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        switch (gameState) {
            case IDLE:

                // do nothing.

                break;

            case PLACE_TOWERS:

                // Wait for 30 seconds, - time for algorithm, allow
                // for pathfinding to find optimum path
                // then start the game.

                if (System.currentTimeMillis() - startTimerTime >= 5000) {// 29950) {
                    Utils.print("Release the monsters!");
                    AStar pathFinder = new AStar(20, 16);
                    optimalPath = getPath(pathFinder.findPath(passableMap));

                    enemies.add(new Enemy(optimalPath, new Hellspawn()));

                    x = optimalPath.get(optimalPath.size() - 1)[0];
                    y = optimalPath.get(optimalPath.size() - 1)[1] - 32;

                    gameState = GameState.PLAY;
                }

                break;

            case PLAY:

                sprite.update(delta);
                enemies.get(0).move(delta);

                if (enemies.size() == 0) {
                    Utils.print("Enemy list depleted!");
                    gameState = GameState.PLACE_TOWERS;
                    break;
                }

                x = (int)enemies.get(0).x;
                y = (int)enemies.get(0).y;

                break;

            case OVER:

                break;

            default:
                throw new RuntimeException("Invalid gameState: " + gameState);
        }
    }

    @Override
    public int getID() {

        return id;
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {

        testMap.render(0, 0);
        g.drawString("Howdy!", 32*8, 10);

        sprite.draw((int)x, (int)y);
    }

    private void startGame() {
        startTimerTime = System.currentTimeMillis();
        gameState = GameState.PLACE_TOWERS;

        Utils.print("gameState is now: " + gameState.toString());
    }

    private void createAnimations() throws SlickException {

        // Create sprite anims
        //Image [] movementUp = {new Image("data/wmg1_bk1.png"), new Image("data/wmg1_bk2.png")};
        //Image [] movementDown = {new Image("data/wmg1_fr1.png"), new Image("data/wmg1_fr2.png")};
        //Image [] movementLeft = {new Image("data/wmg1_lf1.png"), new Image("data/wmg1_lf2.png")};
        Image [] movementRight = {
                new Image("media/images/demon/demon_right_1.png"),
                new Image("media/images/demon/demon_right_2.png"),
                new Image("media/images/demon/demon_right_3.png"),
                new Image("media/images/demon/demon_right_4.png"),
                new Image("media/images/demon/demon_right_5.png"),
                new Image("media/images/demon/demon_right_6.png"),
                new Image("media/images/demon/demon_right_7.png"),
                new Image("media/images/demon/demon_right_8.png"),
                new Image("media/images/demon/demon_right_9.png")
        };

        int [] duration = {100, 100, 100, 100, 100, 100, 100, 100, 100};

        right = new Animation(movementRight, duration, false);
        sprite = right;
    }

    private ArrayList<int[]> getPath(Node n) {
        ArrayList<int[]> path = new ArrayList<int[]>();

        int cols = 16;

        while(n.predecessor != null) {
            path.add(new int[]{(n.index % cols) * 32, (n.index / cols) * 32});
            n = n.predecessor;
        }

        return path;
    }
}
