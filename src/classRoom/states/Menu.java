package classRoom.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 * @author pieslicer
 */
public class Menu extends BasicGameState {

    // This state
    private final int id = 0;
    // Reference to the container
    private StateBasedGame game;

    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        this.game = game;
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {

        // Render a cool 80's-kind-of menu
        g.setColor(Color.white);
        g.drawString("Fantasy Creature TD", 50, 10);

        g.drawString("1. Play Game", 50, 100);
        g.drawString("2. Settings", 50, 120);
        g.drawString("3. Quit", 50, 140);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {

    }

    @Override
    public int getID() {
        return id;
    }
    public void keyReleased(int key, char c) {
        switch(key) {
            case Input.KEY_1:
                // Go to InGame.
                game.enterState(1, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
                break;
            case Input.KEY_2:

                break;
            case Input.KEY_3:

                System.exit(0);
                break;
            default:
                break;
        }
    }
}
