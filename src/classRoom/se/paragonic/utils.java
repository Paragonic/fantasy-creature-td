package classRoom.se.paragonic;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Pieslicer
 */
public class Utils {

    private static Calendar cal = Calendar.getInstance();
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Utils (){

    }

    public static void print(String s) {
        System.out.println(sdf.format(cal.getTime()) + " [LOG]: " + s);
    }
}
