package classRoom.gameObjects.species;

import classRoom.se.paragonic.Utils;

/**
 * Created on 12/11/14.
 *
 * @author maxjonsson
 */
public class Species {

    public String name = "";
    public String info = "";

    public Species(String _name, String _info) {
        this.name = _name;
        this.info = _info;

        Utils.print("A " + this.name + " was spawned!");
    }
}
