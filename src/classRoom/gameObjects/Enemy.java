package classRoom.gameObjects;

import java.util.ArrayList;
import java.util.Collections;

import classRoom.gameObjects.species.Species;
import classRoom.se.paragonic.Utils;
import classRoom.states.InGame;

/**
 * @author pieslicer
 */
public class Enemy {

    protected ArrayList<int[]> path;
    private Species species;

    public double x = 0;
    public double y = 0;

    private double speed = 0.2f;

    public Enemy(ArrayList<int[]> _path, Species _s) {
        path = _path;
        species = _s;

        Collections.reverse(path);

        Utils.print("[" + species.name + "] was spawned!");
    }

    public void move(int delta) {
        if (y - path.get(0)[1] < 0) {
            this.y += speed * delta;
        } else {
            this.y -= speed * delta;
        }

        if (x - path.get(0)[0] > 0) {
            this.x -= speed * delta;
        } else {
            this.x += speed * delta;
        }

        if (Math.sqrt(Math.pow(x - path.get(0)[0], 2) + Math.pow(y - path.get(0)[1], 2)) < 1) {
            Utils.print("Popped (" + path.get(0)[0] + ", " + path.get(0)[1] + ") off the stack!");
            path.remove(0);

            if (path.size() == 0) {
                this.destroy();
            }
        }
    }

    protected void destroy() {
        InGame.enemies.remove(this);
    }
}
