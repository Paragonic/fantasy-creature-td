package classRoom;

import classRoom.gameObjects.species.*;
import classRoom.se.paragonic.Utils;

/**
 * @author maxjonsson
 */
public class GameMaster {

    // CONSTRUCTOR
    public GameMaster() {
        Utils.print("The game master is alive!");
        // This class should be used for handling the different races.
        // Basically it keeps track of info about players.
    }
}
