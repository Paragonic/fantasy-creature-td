import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import classRoom.states.*;

/**
 * Coder: pieslicer
 */
public class Engine extends StateBasedGame
{

    private float x, y;

    public Engine(String gamename) {
        super(gamename);
    }


    @Override
    public void initStatesList(GameContainer g) throws SlickException {
        addState(new Menu());
        addState(new InGame());
    }

    public static void main(String[] args)
    {
        try
        {
            AppGameContainer appgc;
            appgc = new AppGameContainer(new Engine("Fantasy Creature TD"));
            appgc.setDisplayMode(32*16, 32*20, false);
            appgc.start();
        }
        catch (SlickException ex)
        {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
